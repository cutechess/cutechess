#include "ChessEngine.h"

#include <QtDebug>
#include "PositionListModel.h"

ChessEngine::ChessEngine(QObject *parent) :
    QObject(parent),
    m_board(8, QVector<Piece*>(8, 00)),
    m_selectedPiece(00),
    m_pawnMakesFirstMove(-1, -1),
    m_isWhiteToPlay(true),
    m_whiteCheck(false),
    m_blackCheck(false),
    m_checkmate(false),
    m_pat(false),
    m_availablePositionsModel(new PositionListModel(this)),
    m_threatPositionsModel(new PositionListModel(this))
{
    // Populate the board with Piece in start position
    m_board[0][0] = new Piece(Piece::Rook, QPoint(0, 0), true, this);
    m_board[0][1] = new Piece(Piece::Knight, QPoint(0, 1), true, this);
    m_board[0][2] = new Piece(Piece::Bishop, QPoint(0, 2), true, this);
    m_board[0][3] = new Piece(Piece::Queen, QPoint(0, 3), true, this);
    m_board[0][4] = new Piece(Piece::King, QPoint(0, 4), true, this);
    m_board[0][5] = new Piece(Piece::Bishop, QPoint(0, 5), true, this);
    m_board[0][6] = new Piece(Piece::Knight, QPoint(0, 6), true, this);
    m_board[0][7] = new Piece(Piece::Rook, QPoint(0, 7), true, this);
    for (int i = 0; i < 8; i++) m_board[1][i] = new Piece(Piece::Pawn, QPoint(1, i), true, this);

    m_board[7][0] = new Piece(Piece::Rook, QPoint(7, 0), false, this);
    m_board[7][1] = new Piece(Piece::Knight, QPoint(7, 1), false, this);
    m_board[7][2] = new Piece(Piece::Bishop, QPoint(7, 2), false, this);
    m_board[7][3] = new Piece(Piece::Queen, QPoint(7, 3), false, this);
    m_board[7][4] = new Piece(Piece::King, QPoint(7, 4), false, this);
    m_board[7][5] = new Piece(Piece::Bishop, QPoint(7, 5), false, this);
    m_board[7][6] = new Piece(Piece::Knight, QPoint(7, 6), false, this);
    m_board[7][7] = new Piece(Piece::Rook, QPoint(7, 7), false, this);
    for (int i = 0; i < 8; i++) m_board[6][i] = new Piece(Piece::Pawn, QPoint(6, i), false, this);

    m_whiteKing = m_board[0][4];
    m_blackKing = m_board[7][4];
}

ChessEngine::~ChessEngine()
{
}

Piece *ChessEngine::pieceAt(const QPoint &position) const
{
    if (!Piece::isPositionValid(position)) return 00;
    return m_board[position.x()][position.y()];
}

Piece *ChessEngine::pieceAt(int x, int y) const
{
    return pieceAt(QPoint(x, y));
}

bool ChessEngine::isPositionFree(const QPoint &position) const
{
    return pieceAt(position) == 00;
}

QList<QObject*> ChessEngine::getAllPieces() const
{
    QList<QObject*> all;
    for (int x = 0; x < 8; x++) {
        for (int y = 0; y < 8; y++) {
            if (m_board[x][y] != 00) all.append(m_board[x][y]);
        }
    }
    foreach(Piece* piece, m_capturedPieces) all.append(piece);
    return all;
}

QList<QPoint> ChessEngine::availablePositionsFrom(int x, int y)
{
    return availablePositionsFrom(pieceAt(x, y));
}

bool ChessEngine::isAvailableMove(int x, int y)
{
    return m_availablePositionsModel->contains(QPoint(x, y));
}

int ChessEngine::pieceTypeAt(int x, int y)
{
    if (isPositionFree(QPoint(x, y))) return Piece::Undefined;
    return pieceAt(x, y)->type();
}

bool ChessEngine::pieceIsWhiteAt(int x, int y)
{
    if (isPositionFree(QPoint(x, y))) return false;
    return pieceAt(x, y)->isWhite();
}


/**
  * List all points where the piece is allowed to move
  */
QList<QPoint> ChessEngine::availablePositionsFrom(Piece *piece)
{
    if (piece == 00) return QList<QPoint>();
    QList<QPoint> validPositions;

    // First add all available positions
    if (piece->type() == Piece::Pawn) {
        validPositions.append(pawnMovePositionsFrom(piece));
        validPositions.append(pawnCapturePositionsFrom(piece));
    }
    if (piece->type() == Piece::Rook || piece->type() == Piece::Queen) {
        validPositions.append(rookAvailablePositionsFrom(piece));
    }
    if (piece->type() == Piece::Knight) {
        validPositions.append(knightAvailablePositionsFrom(piece));
    }
    if (piece->type() == Piece::Bishop || piece->type() == Piece::Queen) {
        validPositions.append(bishopAvailablePositionsFrom(piece));
    }
    if (piece->type() == Piece::King) {
        validPositions.append(kingAvailablePositionsFrom(piece));

        // Check for roques moves
        if (piece->isFirstMove()) {
            int line = piece->isWhite() ? 0 : 7;
            if (isSmallRoque(piece->isWhite())) validPositions.append(QPoint(line, 6));
            if (isBigRoque(piece->isWhite())) validPositions.append(QPoint(line, 2));
        }
    }

    // Check that each of these moves wont get our king in check !
    Piece* king = piece->isWhite() ? m_whiteKing : m_blackKing;
    foreach(QPoint pos, validPositions) {
        if (!threatList(king, piece, pos).isEmpty()) validPositions.removeOne(pos);
    }

    return validPositions;
}

/**
  * List all positions that threaten threatenedPiece.
  */
QList<QPoint> ChessEngine::threatList(Piece *threatenedPiece) {
    if (threatenedPiece == 00) return QList<QPoint>();
    return threatList(threatenedPiece->position(), threatenedPiece->isWhite());
}

QList<QPoint> ChessEngine::threatList(const QPoint &threatenedPosition, bool threatenedIsWhite)
{
    QList<QPoint> threats;
    for (int row = 0; row < 8; row++) {
        for (int column = 0; column < 8; column++) {
            QPoint threat(row, column);
            if (isPositionFree(threat)) continue;
            if (pieceAt(threat)->isWhite() == threatenedIsWhite) continue;

            QList<QPoint> threatMoves;
            if (pieceAt(threat)->type() == Piece::Pawn) {
                threatMoves.append(pawnCapturePositionsFrom(pieceAt(threat)));
                if (isCaptureEnPassant(pieceAt(threat), threatenedPosition + QPoint(threatenedIsWhite ? -1 : 1, 0)))
                    threats.append(threat);
            }
            if (pieceAt(threat)->type() == Piece::Rook || pieceAt(threat)->type() == Piece::Queen)
                threatMoves.append(rookAvailablePositionsFrom(pieceAt(threat)));
            if (pieceAt(threat)->type() == Piece::Knight)
                threatMoves.append(knightAvailablePositionsFrom(pieceAt(threat)));
            if (pieceAt(threat)->type() == Piece::Bishop || pieceAt(threat)->type() == Piece::Queen)
                threatMoves.append(bishopAvailablePositionsFrom(pieceAt(threat)));
            if (pieceAt(threat)->type() == Piece::King) threatMoves.append(kingAvailablePositionsFrom(pieceAt(threat)));
            if (threatMoves.contains(threatenedPosition)) threats.append(threat);
        }
    }
    return threats;
}

QList<QPoint> ChessEngine::threatList(Piece *threatenedPiece, Piece *movingPiece, const QPoint &movingTo)
{
    if (threatenedPiece == movingPiece) return threatList(movingTo, threatenedPiece->isWhite(), movingPiece, movingTo);
    return threatList(threatenedPiece->position(), threatenedPiece->isWhite(), movingPiece, movingTo);
}

/**
  * List all positions that threaten threatenedPiece, considered movingPiece is going to a new position.
  */
QList<QPoint> ChessEngine::threatList(const QPoint& threatenedPosition, bool threatenedIsWhite, Piece* movingPiece, const QPoint &movingTo)
{
    // Consider the move is done to evaluate the threat
    QPoint pawnState = m_pawnMakesFirstMove;
    QList<Piece*> movingPieces = movePieceAndCapture(movingPiece, movingTo, true);        // Warning this will trig real piece moves...

    QList<QPoint> threats = threatList(threatenedPosition, threatenedIsWhite);

    // Roll back positions
    foreach(Piece* piece, movingPieces) {
        if (piece->captured()) m_capturedPieces.removeOne(piece);
        if (pieceAt(piece->position()) == piece) setPieceAt(piece->position(), 00);
        piece->rollback();
        setPieceAt(piece->position(), piece);
        piece->blockSignals(false);
    }
    m_pawnMakesFirstMove = pawnState;

    return threats;
}

/**
  * Move the piece, check for capture, do promotions, then update check and checkmate conditions.
  * Do not verify that the move is legal, could be done if we need to interface unsafe interface.
  * If promotion occurs, piece is promoted to promoteType, GUI should ask the user before
  * calling this function.
  */
bool ChessEngine::movePieceTo(Piece *piece, const QPoint &pos, Piece::ChessType promoteType)
{
    if (piece == 00) return false;

    QList<Piece*> movingPieces = movePieceAndCapture(piece, pos);
    if (movingPieces.isEmpty()) return false;

    // Promotion
    if ((piece->type() == Piece::Pawn) && piece->position().x() == (piece->isWhite() ? 7 : 0)) {
        piece->setType(promoteType);
    }

    // Check conditions - the move may get opponent in check state but also make our king not in check anymore
    if (piece->isWhite() && !threatList(m_blackKing).isEmpty()) setBlackCheck(true);
    else setBlackCheck(false);
    if (!piece->isWhite() && !threatList(m_whiteKing).isEmpty()) setWhiteCheck(true);
    else setWhiteCheck(false);

    // Checkmate condition only for opponent, check have to be set.
    if (whiteCheck() && !hasAvailableMove(true)) setCheckmate(true);
    if (blackCheck() && !hasAvailableMove(false)) setCheckmate(true);

    // Pat condition
    if (!checkmate() && !hasAvailableMove(!isWhiteToPlay())) setPat(true);

    return true;
}

bool ChessEngine::hasAvailableMove(bool white) {
    // Loop on all piece to find if there is a move available
    bool foundMove = false;
    for (int row = 0; row < 8; row++) {
        for (int column = 0; column < 8; column++) {
            QPoint position(row, column);
            if (isPositionFree(position)) continue;
            if (pieceAt(position)->isWhite() != white) continue;
            if (!availablePositionsFrom(pieceAt(position)).isEmpty()) {
                foundMove = true;
                break;
            }
        }
        if (foundMove) break;
    }
    return foundMove;
}

void ChessEngine::setSelectedPosition(int x, int y)
{
    m_availablePositionsModel->setPositions(availablePositionsFrom(pieceAt(x, y)));
    if (m_selectedPiece != 00) m_selectedPiece->setSelected(false);
    m_selectedPiece = pieceAt(x, y);
    if (m_selectedPiece) m_selectedPiece->setSelected(true);
}

void ChessEngine::setThreatPosition(int x, int y)
{
    m_threatPositionsModel->setPositions(threatList(pieceAt(x, y)));
}

void ChessEngine::setThreatMovingPiece(int posx, int posy, int movingx, int movingy)
{
    m_threatPositionsModel->setPositions(
                threatList(QPoint(movingx, movingy), m_isWhiteToPlay,
                           pieceAt(posx, posy), QPoint(movingx, movingy)));
}

void ChessEngine::clearThreats()
{
    m_threatPositionsModel->clear();
}

void ChessEngine::movePiece(int xFrom, int yFrom, int xTo, int yTo, int promoteType)
{
    movePieceTo(pieceAt(xFrom, yFrom), QPoint(xTo, yTo), (Piece::ChessType)promoteType);
    setWhiteToPlay(!isWhiteToPlay());
}

void ChessEngine::setWhiteToPlay(bool arg)
{
    if (m_isWhiteToPlay != arg) {
        m_isWhiteToPlay = arg;
        emit isWhiteToPlayChanged(arg);
    }
}

void ChessEngine::setWhiteCheck(bool arg)
{
    if (m_whiteCheck != arg) {
        m_whiteCheck = arg;
        emit whiteCheckChanged(arg);
    }
}

void ChessEngine::setBlackCheck(bool arg)
{
    if (m_blackCheck != arg) {
        m_blackCheck = arg;
        emit blackCheckChanged(arg);
    }
}

void ChessEngine::setCheckmate(bool arg)
{
    if (arg) {
        setWhiteCheck(false);
        setBlackCheck(false);
    }
    if (m_checkmate != arg) {
        m_checkmate = arg;
        emit checkmateChanged(arg);
    }
}

void ChessEngine::setPat(bool arg)
{
    if (m_pat != arg) {
        m_pat = arg;
        emit patChanged(arg);
    }
}

void ChessEngine::setPieceAt(const QPoint &position, Piece *piece)
{
    if (!Piece::isPositionValid(position)) return;
    m_board[position.x()][position.y()] = piece;
}

void ChessEngine::capturePosition(const QPoint &position)
{
    if (!Piece::isPositionValid(position)) return;
    if (isPositionFree(position)) return;

    Piece* capturedPiece = pieceAt(position);
    if (capturedPiece == 00) return;

    m_capturedPieces.append(capturedPiece);
    setPieceAt(position, 00);
    capturedPiece->capture();
}

bool ChessEngine::isCaptureEnPassant(const Piece *piece, const QPoint &movingTo) const
{
    if (!Piece::isPositionValid(m_pawnMakesFirstMove)) return false;
    if (piece == 00) return false;
    if (piece->type() != Piece::Pawn) return false;

    int direction = piece->isWhite() ? 1 : -1;
    if (piece->position() - movingTo != QPoint(-direction, 1) && piece->position() - movingTo != QPoint(-direction, -1)) return false;
    if (movingTo == m_pawnMakesFirstMove + QPoint(direction, 0)) return true;
    return false;
}

// Return the list of pieces that moved (captured included)
QList<Piece *> ChessEngine::movePieceAndCapture(Piece *piece, const QPoint &pos, bool fakeMove) {
    QList<Piece*> movingPieces;
    if (!Piece::isPositionValid(pos)) return movingPieces;
    if (piece == 00) return movingPieces;

    // Capture en passant
    if (isCaptureEnPassant(piece, pos)) {
        movingPieces.append(pieceAt(m_pawnMakesFirstMove));
        if (fakeMove) pieceAt(m_pawnMakesFirstMove)->blockSignals(true);
        capturePosition(m_pawnMakesFirstMove);
    }

    // Note when a pawn makes a move that could enable en passant capture next round
    if (piece->type() == Piece::Pawn && abs(piece->position().x() - pos.x()) == 2) m_pawnMakesFirstMove = pos;
    else m_pawnMakesFirstMove = QPoint(-1, -1);

    // Regular capture
    if (!isPositionFree(pos)) {
        movingPieces.append(pieceAt(pos));
        if (fakeMove) pieceAt(pos)->blockSignals(true);
        capturePosition(pos);
    }

    // Check for Roques moves
    if (piece->type() == Piece::King && abs(pos.y() - piece->position().y()) > 1) {
        // Small Roque move
        if (pos.y() > piece->position().y()) {
            Piece* rook = pieceAt(piece->position().x(), 7);
            Q_ASSERT(rook);
            setPieceAt(rook->position(), 00);
            setPieceAt(QPoint(piece->position().x(), 5), rook);
            if (fakeMove) rook->blockSignals(true);
            rook->moveTo(QPoint(piece->position().x(), 5));
            movingPieces.append(rook);
        }
        // Big Roque move
        if (pos.y() < piece->position().y()) {
            Piece* rook = pieceAt(piece->position().x(), 0);
            Q_ASSERT(rook);
            setPieceAt(rook->position(), 00);
            setPieceAt(QPoint(piece->position().x(), 3), rook);
            if (fakeMove) rook->blockSignals(true);
            rook->moveTo(QPoint(piece->position().x(), 3));
            movingPieces.append(rook);
        }
    }

    // Move the piece
    movingPieces.append(piece);
    if (fakeMove) piece->blockSignals(true);
    setPieceAt(piece->position(), 00);
    setPieceAt(pos, piece);
    piece->moveTo(pos);

    return movingPieces;
}

bool ChessEngine::isSmallRoque(bool isWhite)
{
    int line = isWhite ? 0 : 7;
    if (pieceAt(line, 4) == 00 || !pieceAt(line, 4)->isFirstMove()) return false;
    if (pieceAt(line, 7) == 00 || !pieceAt(line, 7)->isFirstMove()) return false;
    if (pieceAt(line, 5) != 00 || pieceAt(line, 6) != 00) return false;
    if (!threatList(pieceAt(line, 4)).isEmpty()) return false;
    if (!threatList(QPoint(line, 5), isWhite).isEmpty()) return false;
    if (!threatList(QPoint(line, 6), isWhite).isEmpty()) return false;
    return true;
}

bool ChessEngine::isBigRoque(bool isWhite)
{
    int line = isWhite ? 0 : 7;
    if (pieceAt(line, 4) == 00 || !pieceAt(line, 4)->isFirstMove()) return false;
    if (pieceAt(line, 0) == 00 || !pieceAt(line, 0)->isFirstMove()) return false;
    if (pieceAt(line, 3) != 00 || pieceAt(line, 2) != 00 || pieceAt(line, 1) != 00) return false;
    if (!threatList(pieceAt(line, 4)).isEmpty()) return false;
    if (!threatList(QPoint(line, 3), isWhite).isEmpty()) return false;
    if (!threatList(QPoint(line, 2), isWhite).isEmpty()) return false;
    return true;
}

bool ChessEngine::isFreeOrOppositeColor(const QPoint &move, bool isWhite) const
{
    return Piece::isPositionValid(move) && (isPositionFree(move) || (!isPositionFree(move) && pieceAt(move)->isWhite() != isWhite));
}

QList<QPoint> ChessEngine::rookAvailablePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;

    move = piece->position() + QPoint(1, 0);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(1, 0);
    }
    move = piece->position() + QPoint(-1, 0);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(-1, 0);
    }
    move = piece->position() + QPoint(0, 1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(0, 1);
    }
    move = piece->position() + QPoint(0, -1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(0, -1);
    }

    return validPositions;
}

QList<QPoint> ChessEngine::bishopAvailablePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;

    move = piece->position() + QPoint(1, 1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(1, 1);
    }
    move = piece->position() + QPoint(-1, -1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(-1, -1);
    }
    move = piece->position() + QPoint(1,- 1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(1, -1);
    }
    move = piece->position() + QPoint(-1, 1);
    while (Piece::isPositionValid(move)) {
        if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
        if (!isPositionFree(move)) break;
        move = move + QPoint(-1, 1);
    }

    return validPositions;
}

QList<QPoint> ChessEngine::knightAvailablePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;

    move = piece->position() + QPoint(1, 2);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-1, 2);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(1, -2);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-1, -2);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(2, 1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-2, 1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(2, -1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-2, -1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);

    return validPositions;
}

QList<QPoint> ChessEngine::kingAvailablePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;

    move = piece->position() + QPoint(1, 1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(1, 0);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(1, -1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-1, 1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-1, 0);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(-1, -1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(0, 1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);
    move = piece->position() + QPoint(0, -1);
    if (isFreeOrOppositeColor(move, piece->isWhite())) validPositions.append(move);

    return validPositions;
}

QList<QPoint> ChessEngine::pawnMovePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;
    int direction = piece->isWhite() ? 1 : -1;

    move = piece->position() + QPoint(direction, 0);
    if (Piece::isPositionValid(move) && isPositionFree(move)) {
        validPositions.append(move);
        if (piece->isFirstMove()) {
            move = piece->position() + QPoint(2 * direction, 0);
            if (Piece::isPositionValid(move) && isPositionFree(move)) validPositions.append(move);
        }
    }

    return validPositions;
}

QList<QPoint> ChessEngine::pawnCapturePositionsFrom(const Piece *piece) const
{
    QList<QPoint> validPositions;
    QPoint move;
    int direction = piece->isWhite() ? 1 : -1;

    // Capture moves
    move = piece->position() + QPoint(direction, 1);
    if (Piece::isPositionValid(move) && !isPositionFree(move) && (pieceAt(move)->isWhite() != piece->isWhite())) validPositions.append(move);
    if (isCaptureEnPassant(piece, move)) validPositions.append(move);
    move = piece->position() + QPoint(direction, -1);
    if (Piece::isPositionValid(move) && !isPositionFree(move) && (pieceAt(move)->isWhite() != piece->isWhite())) validPositions.append(move);
    if (isCaptureEnPassant(piece, move)) validPositions.append(move);


    //    if (Piece::isPositionValid(m_pawnMakesFirstMove)) {
    //        if (m_pawnMakesFirstMove == piece->position() + QPoint(0, 1)) validPositions.append(piece->position() + QPoint(direction, 1));
    //        if (m_pawnMakesFirstMove == piece->position() + QPoint(0, -1)) validPositions.append(piece->position() + QPoint(direction, -1));
    //    }

    return validPositions;
}
