#include "Piece.h"

#include <QtDebug>

// Default constructor required for QDeclarative
Piece::Piece(QObject *parent) :
    QObject(parent),
    m_type(Pawn),
    m_position(-1, -1),
    m_lastPosition(-1, -1),
    m_lastLastPosition(-1, -1),
    m_isWhite(true),
    m_selected(false)
{
}

Piece::Piece(ChessType type, const QPoint &pos, bool isWhite, QObject *parent) :
    QObject(parent),
    m_type(type),
    m_position(pos),
    m_lastPosition(-1, -1),
    m_lastLastPosition(-1, -1),
    m_isWhite(isWhite),
    m_selected(false)
{
}

Piece::~Piece()
{
}

void Piece::setType(Piece::ChessType arg)
{
    if (m_type != arg) {
        m_type = arg;
        emit typeChanged(arg);
    }
}

void Piece::moveTo(const QPoint &position)
{
    if (!isPositionValid(position)) return;
    if (m_position == position) return;

    m_lastLastPosition = m_lastPosition;
    m_lastPosition = m_position;
    setPosition(position);
}

void Piece::capture()
{
    m_lastLastPosition = m_lastPosition;
    m_lastPosition = m_position;
    setPosition(QPoint(-1, -1));
}

/**
  * Rollback last move. Can only rollback one move (capture move also)
  * Used by engine to simulate moves and computes possible threats.
  */
void Piece::rollback()
{
    if (!isPositionValid(m_lastPosition)) return;

    setPosition(m_lastPosition);
    m_lastPosition = m_lastLastPosition;
}

void Piece::setSelected(bool arg)
{
    if (m_selected != arg) {
        m_selected = arg;
        emit selectedChanged(arg);
    }
}

void Piece::setPosition(QPoint arg)
{
    bool wasCaptured = captured();

    if (m_position != arg) {
        m_position = arg;
        emit positionChanged(arg);
    }

    if (captured() != wasCaptured) emit capturedChanged(captured());
}

bool Piece::isPositionValid(const QPoint &position)
{
    return position.x() >= 0 && position.x() < 8 && position.y() >= 0 && position.y() < 8;
}
