#include <QtGui/QApplication>
#include <QtDeclarative>
#include "qdeclarativeview3d.h"
#include "PositionListModel.h"
#include "ChessEngine.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QString locale = QLocale::system().name();
    QTranslator* translator = new QTranslator;
    QString translationFile = QString("qml/CuteChess/i18n/qml_") + locale + QString(".qm");
    qWarning() << "Translating: " << translationFile << translator->load(translationFile);
    app.installTranslator(translator);

    qRegisterMetaType<Piece::ChessType>("Piece::ChessType");
    qmlRegisterType<Piece>("CuteChess", 1, 0, "PieceModel");
    qmlRegisterType<ChessEngine>("CuteChess", 1, 0, "ChessEngine");
    qmlRegisterType<PositionListModel>();

    QDeclarativeView3D view;
    view.setSource(QUrl(QLatin1String("qml/CuteChess/main.qml")));

#ifdef Q_OS_SYMBIAN
    view.setAttribute(Qt::WA_LockLandscapeOrientation, true);
    view.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    view.showMaximized();
#else
    view.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    view.setMinimumSize(600, 400);
    view.move(200, 100);

    if (QApplication::arguments().contains(QLatin1String("-maximize")))
        view.showMaximized();
    else if (QApplication::arguments().contains(QLatin1String("-fullscreen")))
        view.showFullScreen();
    else
        view.show();
#endif

    QObject::connect(view.engine(), SIGNAL(quit()), QCoreApplication::instance(), SLOT(quit()));

    return app.exec();
}
