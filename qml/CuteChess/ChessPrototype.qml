// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import Qt3D.Shapes 1.0

Viewport {
    id: viewport
    width: 800;
    height: 600
    picking: true

    camera: Camera {
        id: camera
        eye: Qt.vector3d(0, 200, 170)
//        eye: Qt.vector3d(0, 200, 1)     // top camera
    }

    Chessboard {

    }

    AvailablePosition {
        row: 0
        column: 0
        color: "#900000FF"
    }

    AvailablePosition {
        row: 4
        column: 4
        color: "#900000FF"
    }

    AvailablePosition {
        row: 7
        column: 7
        color: "#900000FF"
    }

    Pawn {
        row: 1
        column: 4
    }

    Pawn {
        row: 1
        column: 5
    }

    Knight {
        row: 0
        column: 5
        onPieceClicked: { row = 2; column = 4;}
    }

    Bishop {
        row: 0
        column: 0
        onPieceClicked: { row = 7; column = 7; }
    }

    Rook {
        row: 2
        column: 0
        onPieceClicked: { row = 7; column = 0; }
    }
    Rook {
        isWhite: false
        row: 7
        column: 6
        onPieceClicked: { row = 0; column = 6; }
    }

}
