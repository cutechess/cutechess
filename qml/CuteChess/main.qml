// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: root
    width: 800
    height: 600

    MenuBar {
        id: menubar
        anchors.left: root.left
        anchors.top: root.top
        anchors.bottom: root.bottom

        onNewGameClicked: {
            gameLoader.source = "";
            loaderTimer.start();
        }
    }

    Loader {
        id: gameLoader
        anchors.left: menubar.right
        anchors.top: root.top
        anchors.bottom: root.bottom
        anchors.right: root.right

        Binding { target: gameLoader.item; property: "rotate3Denabled"; value: menubar.rotate3Denabled; }
        Binding { target: gameLoader.item; property: "topViewEnabled"; value: menubar.topViewEnabled; }
        Binding { target: gameLoader.item; property: "showThreatsEnabled"; value: menubar.showThreatsEnabled; }
    }

    Timer {
        id: loaderTimer
        interval: 20
        onTriggered: gameLoader.source = "GameArea.qml"
    }

    Component.onCompleted: {
        loaderTimer.start()
    }
}
