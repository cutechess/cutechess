.pragma library

var whiteCaptureCount = 0;
var blackCaptureCount = 0;

var selectedPosition_x = -1;
var selectedPosition_y = -1;

var move_x = -1;
var move_y = -1;

function setSelectedPosition(x, y)
{
    var changed = (selectedPosition_x !== x) || (selectedPosition_y !== y);

    selectedPosition_x = x;
    selectedPosition_y = y;

    return changed;
}

function clearSelectedPosition()
{
    selectedPosition_x = -1;
    selectedPosition_y = -1;
}

function isSelectionValid()
{
    if (selectedPosition_x < 0 || selectedPosition_y < 0) return false;
    else return true;
}
