// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Piece {
    id: root
    mesh: Mesh { source: "obj/King.obj" }

    pretransform: Translation3D {
        translate: Qt.vector3d(-0.871,  -6.1, -28.5360)
    }

    Behavior on x { NumberAnimation { duration: Graphics.MovesDuration } }
    Behavior on z { NumberAnimation { duration: Graphics.MovesDuration } }
}
