// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Piece {
    mesh: Mesh { source: "obj/Rook.obj" }

    pretransform: Translation3D {
        translate: Qt.vector3d(-23.916,  -6.1, -28.906)
    }

    transform: [
        Rotation3D {
            id: xRotation
            axis: Qt.vector3d(0, 0, 1)
        },
        Rotation3D {
            id: zRotation
            axis: Qt.vector3d(1, 0, 0)
        }
    ]

    Item {
        id: p   // private data
        property real bounceAngle: 15
        property real bounceDuration: Graphics.MovesDuration * 0.125
        property real moveDuration: Graphics.MovesDuration * 0.75
    }

    Behavior on x {
            NumberAnimation { duration: Graphics.MovesDuration }
    }

    Behavior on z {
        SequentialAnimation {
            PropertyAnimation {
                target: zRotation;
                property: "angle";
                to: isWhite ? -p.bounceAngle : p.bounceAngle
                duration: p.bounceDuration
                easing.type: Easing.InBack
            }

            NumberAnimation { duration: p.moveDuration }

            PropertyAnimation {
                target: zRotation;
                property: "angle";
                to: 0
                duration: p.bounceDuration
                easing.type: Easing.OutBack
            }
        }
    }
}
