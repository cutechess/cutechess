// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Item3D {
    id: root
    property bool isWhite: true
    property int row: 0
    property int column: 0
    property bool selected: false
    signal pieceClicked()
    signal pieceHoverEnter()
    signal pieceHoverLeave()

    // Warning: effect is the same for all mesh, meshnodes can't have different effect.
    effect: Effect {
        texture: isWhite ? "textures/lightWood.jpg" : "textures/darkWood.jpg";
    }

    onClicked: pieceClicked()
    onHoverEnter: pieceHoverEnter()
    onHoverLeave: pieceHoverLeave()

    x: Graphics.cellCenter_X(column)
    y: Graphics.BoardTopSurfaceOffset
    z: Graphics.cellCenter_Z(row)

    SequentialAnimation on scale {
        running: selected
        alwaysRunToEnd: true
        loops: Animation.Infinite
        PropertyAnimation { to: 1.3; duration: 500 }
        PropertyAnimation { to: 1.0; duration: 500 }
    }
}
