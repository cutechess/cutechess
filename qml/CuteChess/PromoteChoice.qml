// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0

Rectangle {
    id: root
    property bool isWhite: true
    property int selectedIndex: 3
    signal validated(int selectedIndex)

    width: 800
    height: 600
    color: "#a09e9e"
    opacity: 0.7
    anchors.fill: parent

    focus: true
    Keys.onRightPressed: {
        selectedIndex = selectedIndex + 1;
        if (selectedIndex > 3) selectedIndex = 0;
        event.accepted = true;
    }

    Keys.onLeftPressed: {
        selectedIndex = selectedIndex - 1;
        if (selectedIndex < 0) selectedIndex = 3
        event.accepted = true;
    }

    Keys.onReturnPressed: root.validated(selectedIndex)
    Keys.onSpacePressed: root.validated(selectedIndex)

    // Prevent clic going through and going into the game
    MouseArea {
        anchors.fill: parent
    }

    Message {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 40
        text: "Choose promotion"
    }

    Rectangle {
        id: rectangle2
        width: 400
        height: 200
        color: "#000000"
        radius: 30
        border.width: 3
        border.color: "darkblue"
        anchors.centerIn: parent

        Viewport {
            anchors.fill: parent
            id: viewport
            picking: true
            navigation: false

            camera: Camera {
                id: camera
                eye: Qt.vector3d(0, 40, 70)
                center: Qt.vector3d(0, 15, 0)
            }

            Knight {
                isWhite: root.isWhite
                row: 4
                column: 2
                transform: Rotation3D {
                    axis: Qt.vector3d(0, 1, 0);
                    angle: root.isWhite ? 180 : 0
                }
                onPieceClicked: selectedIndex = 0;
            }

            Bishop {
                isWhite: root.isWhite
                row: 4
                column: 3
                onPieceClicked: selectedIndex = 1;
            }

            Rook {
                isWhite: root.isWhite
                row: 4
                column: 4
                onPieceClicked: selectedIndex = 2;
            }

            Queen {
                isWhite: root.isWhite
                row: 4
                column: 5
                onPieceClicked: selectedIndex = 3;
            }

            AvailablePosition {
                row: 4
                column: 2 + selectedIndex
                color: "yellow"
            }
        }
    }

    Rectangle {
        id: rectangle1
        border.width: 3
        border.color: "#00008b"
        color: "#000000"
        radius: 25

        anchors.top: rectangle2.bottom
        anchors.topMargin: 30
        anchors.horizontalCenter: parent.horizontalCenter

        width: text1.width + 50
        height: text1.height + 30

        MouseArea {
            anchors.fill: parent
            onClicked: root.validated(selectedIndex)
        }

        Text {
            id: text1
            color: "#ffffff"
            style: Text.Outline
            font.family: "MV Boli"
            font.bold: false
            font.pointSize: 30
            text: qsTr("Continue")
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
