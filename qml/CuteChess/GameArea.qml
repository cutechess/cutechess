// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Item {
    id: root
    property bool rotate3Denabled : false
    property bool topViewEnabled: false
    property alias showThreatsEnabled: chessgame.showThreatsEnabled

    Rectangle {
        anchors.fill: parent
        color: "black"
        border.color: "darkblue"
        border.width: 2
    }

    Message {
        id: whiteCheckMessage
        text: qsTr("White Check")
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: root.top
        anchors.topMargin: 20
        opacity: chessgame.whiteCheck ? 1.0 : 0.0
    }

    Message {
        id: blackCheckMessage
        text: qsTr("Black Check")
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: root.top
        anchors.topMargin: 20
        opacity: chessgame.blackCheck ? 1.0 : 0.0
    }

    Message {
        id: checkmateMessage
        text: qsTr("Checkmate !")
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: root.top
        anchors.topMargin: 20
        opacity: chessgame.checkmate ? 1.0 : 0.0
    }

    Message {
        id: patMessage
        text: qsTr("Pat !")
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: root.top
        anchors.topMargin: 20
        opacity: chessgame.pat ? 1.0 : 0.0
    }

    Viewport {
        id: viewport
        anchors.top: root.top
        anchors.topMargin: 75
        anchors.left: root.left
        anchors.right: root.right
        anchors.bottom: root.bottom
        picking: rotate3Denabled ? false : true
        navigation: rotate3Denabled ? true : false

        camera: Camera {
            id: camera
            eye: topViewEnabled ? Qt.vector3d(0, 300, 1) : Qt.vector3d(0, 200, 170)

            Behavior on eye { PropertyAnimation { duration: 500; } }
        }

        ChessGame {
            id: chessgame

            transform: Rotation3D {
                id: chessgameRotation
                axis: Qt.vector3d(0, 1, 0)
            }

            onChoosePromotionPiece: promoteDialog.opacity = 0.7
        }

        states: [
            State {
                name: "WhitePlayer"
                when: chessgame.isWhiteToPlay
                PropertyChanges {
                    target: chessgameRotation
                    angle: 0
                }
            },
            State {
                name: "BlackPlayer"
                when: !chessgame.isWhiteToPlay
                PropertyChanges {
                    target: chessgameRotation
                    angle: 180
                }
            }
        ]

        transitions: [
            Transition {
                SequentialAnimation {
                    PauseAnimation { duration: Graphics.MovesDuration + 300 }
                    NumberAnimation { target: chessgameRotation; property: "angle"; duration: 800; easing.type: Easing.InOutQuad }
                }
            }
        ]
    }

    PromoteChoice {
        id: promoteDialog
        anchors.fill: parent
        opacity: 0.0

        onValidated: {
            chessgame.makeMoveSelectedTo(selectedIndex)
            opacity = 0.0;
        }
    }

}
