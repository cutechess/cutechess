// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: tool
    property alias iconSource: toolImage.source
    property alias text: toolText.text
    property bool checked: false
    anchors.horizontalCenter: parent.horizontalCenter
    width: 80
    height: 80
    signal clicked()

    Rectangle {
        id: toolRectangle
        color: tool.checked ? "cyan" : "white"
        border.color: "#0c10f1"
        border.width: 2
        radius: 20
        anchors.fill: parent

        Item {
            anchors.top: toolRectangle.top
            anchors.left: toolRectangle.left
            anchors.right: toolRectangle.right
            anchors.bottom: toolText.top
            anchors.bottomMargin: 0

            Image {
                id: toolImage
                width: 48
                height: 48
                fillMode: Image.Stretch
                anchors.centerIn: parent
                smooth: true
            }
        }

        Text {
            id: toolText
            font.pointSize: 10
            anchors.bottom: toolRectangle.bottom
            anchors.bottomMargin: 5
            anchors.horizontalCenter: toolRectangle.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: tool.clicked()
        }
    }
}
