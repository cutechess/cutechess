// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Piece {
    id: knight
    mesh: Mesh { source: "obj/Knight.obj" }

    pretransform: Translation3D {
        translate: Qt.vector3d(-16.272,  -6.1, -28.240)
    }

    transform: Rotation3D {
        axis: Qt.vector3d(0, 1, 0);
        angle: isWhite ? 0 : 180
    }

    Item {
        id: p   // private data
        property real bounceHeight: 12.0
        property real bounceDuration: Graphics.MovesDuration * 0.125
        property real moveDuration: Graphics.MovesDuration * 0.75
    }

    Behavior on x {
        SequentialAnimation {
            PropertyAnimation {
                target: knight;
                property: "y";
                from: Graphics.BoardTopSurfaceOffset;
                to: Graphics.BoardTopSurfaceOffset + p.bounceHeight;
                duration: p.bounceDuration
            }
            NumberAnimation { duration: p.moveDuration }
            PropertyAnimation {
                target: knight;
                property: "y";
                from: Graphics.BoardTopSurfaceOffset + p.bounceHeight;
                to: Graphics.BoardTopSurfaceOffset;
                duration: p.bounceDuration
            }

        }
    }

    Behavior on z {
        SequentialAnimation {
            PauseAnimation { duration: p.bounceDuration }
            NumberAnimation { duration: p.moveDuration }
        }
    }
}
