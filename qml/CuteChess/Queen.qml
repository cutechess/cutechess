// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Piece {
    id: root
    mesh: Mesh { source: "obj/Queen.obj" }

    pretransform: Translation3D {
        translate: Qt.vector3d(6.719,  -6.1, -28.688)
    }

    transform: Rotation3D {
        id: yAxisRotation
        axis: Qt.vector3d(0, 1, 0)
    }

    PropertyAnimation {
        id: yAxisAnimation
        target: yAxisRotation
        property: "angle"
        from: 0
        to: 360
        duration: 200
        loops: Animation.Infinite
        alwaysRunToEnd: true
        running: false
    }


    Behavior on x {
        SequentialAnimation {
            ScriptAction { script: yAxisAnimation.start() }
            NumberAnimation { duration: Graphics.MovesDuration }
            ScriptAction { script: yAxisAnimation.stop() }
        }
    }

    Behavior on z {
        ParallelAnimation {
            ScriptAction { script: yAxisAnimation.start() }
            NumberAnimation { duration: Graphics.MovesDuration }
            ScriptAction { script: yAxisAnimation.stop() }
        }
    }
}
