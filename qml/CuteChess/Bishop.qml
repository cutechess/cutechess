// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import "DrawingConstants.js" as Graphics

Piece {
    mesh: Mesh { source: "obj/Bishop.obj" }

    pretransform: Translation3D {
        translate: Qt.vector3d(-8.585, -6.1, -28.640)
    }

    Behavior on x { NumberAnimation { easing.type: Easing.InOutQuad; duration: Graphics.MovesDuration } }
    Behavior on z { NumberAnimation { easing.type: Easing.OutInQuad; duration: Graphics.MovesDuration } }
}
