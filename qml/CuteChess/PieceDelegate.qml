// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import CuteChess 1.0
import "PieceComponents.js" as Factory
import "ChessState.js" as Logic
import "DrawingConstants.js" as Graphics

/**
  * PieceDelegate is just some glue between PieceModel (C++) and Piece.qml
  * It allows us to choose which qml element to instanciate depending on piece type
  * and also to replace qml component after a piece promotion
  */
Item3D {
    id: root
    property QtObject pieceModel
    property Piece piece
    signal pieceClicked()
    signal pieceHoverEnter()
    signal pieceHoverLeave()

    Component.onCompleted: buildPiece();

    Binding { target: piece; property: "row"; value: pieceModel.position.x; }
    Binding { target: piece; property: "column"; value: pieceModel.position.y; }
    Binding { target: piece; property: "selected"; value: pieceModel.selected; }

    function buildPiece() {
        // Choose the correct component factory
        var factory = null;
        if (pieceModel.type === PieceModel.Pawn) factory = Factory.pawnComponent;
        if (pieceModel.type === PieceModel.Rook) factory = Factory.rookComponent;
        if (pieceModel.type === PieceModel.Knight) factory = Factory.knightComponent;
        if (pieceModel.type === PieceModel.Bishop) factory = Factory.bishopComponent;
        if (pieceModel.type === PieceModel.Queen) factory = Factory.queenComponent;
        if (pieceModel.type === PieceModel.King) factory = Factory.kingComponent;
        if (factory === null) console.log("null factory, cannot create piece");

        // Build the piece
        piece = factory.createObject(root, { "isWhite": pieceModel.isWhite });
        if (piece === null) { console.log("Error creating piece :" + pieceModel.type); }

        // Connect signals of piece
        pieceModel.typeChanged.connect(promotePiece)
        pieceModel.capturedChanged.connect(pieceCaptured)
        piece.pieceClicked.connect(pieceClicked)
        piece.pieceHoverEnter.connect(pieceHoverEnter)
        piece.pieceHoverLeave.connect(pieceHoverLeave)
    }

    function promotePiece() {
        console.log("Promote piece to : " + pieceModel.type);

        piece.destroy();

        buildPiece();
    }

    function pieceCaptured() {
        console.log("piece captured");
        var yoffset = 0;
        if (pieceModel.isWhite) {
            yoffset = Logic.whiteCaptureCount;
            Logic.whiteCaptureCount++;
        }
        else {
            yoffset = Logic.blackCaptureCount;
            Logic.blackCaptureCount++;
        }

        // Break binding, piece wont move after
        piece.x = Graphics.cellCenter_X(-2 + yoffset)
        piece.z = Graphics.cellCenter_Z(pieceModel.isWhite ? 9 : -2)
    }
}
