<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_BE">
<context>
    <name>GameArea</name>
    <message>
        <location filename="../GameArea.qml" line="21"/>
        <source>White Check</source>
        <translation>Echec aux blancs</translation>
    </message>
    <message>
        <location filename="../GameArea.qml" line="30"/>
        <source>Black Check</source>
        <translation>Echec aux noirs</translation>
    </message>
    <message>
        <location filename="../GameArea.qml" line="39"/>
        <source>Checkmate !</source>
        <translation>Echec et mat !</translation>
    </message>
    <message>
        <location filename="../GameArea.qml" line="48"/>
        <source>Pat !</source>
        <translation>Pat !</translation>
    </message>
</context>
<context>
    <name>MenuBar</name>
    <message>
        <location filename="../MenuBar.qml" line="42"/>
        <source>New game</source>
        <translation>Rejouer</translation>
    </message>
    <message>
        <location filename="../MenuBar.qml" line="48"/>
        <source>Rotate 3D</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="../MenuBar.qml" line="55"/>
        <source>Top view</source>
        <translation>Vue dessus</translation>
    </message>
    <message>
        <location filename="../MenuBar.qml" line="62"/>
        <source>Threats</source>
        <translation>Menaces</translation>
    </message>
    <message>
        <location filename="../MenuBar.qml" line="71"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
</context>
</TS>
