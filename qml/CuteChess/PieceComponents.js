.pragma library

// State less javascript is initialized only once when application starts
var pawnComponent = Qt.createComponent("Pawn.qml");
var rookComponent = Qt.createComponent("Rook.qml");
var bishopComponent = Qt.createComponent("Bishop.qml");
var knightComponent = Qt.createComponent("Knight.qml");
var queenComponent = Qt.createComponent("Queen.qml");
var kingComponent = Qt.createComponent("King.qml");
