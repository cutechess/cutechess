.pragma library

var BoardWidth = 80
var BoardHeight = 8
var BoardCenterOffset_X = -BoardWidth / 2.0
var BoardCenterOffset_Z = BoardWidth / 2.0
var BoardCellSpacing = BoardWidth / 8.0
var BoardTopSurfaceOffset = BoardHeight / 2.0
var MovesDuration = 400

function cellCenter_X(column) {
    return BoardCenterOffset_X + BoardCellSpacing * (column + 0.5)
}

function cellCenter_Z(row) {
    return BoardCenterOffset_Z - BoardCellSpacing * (row + 0.5)
}
