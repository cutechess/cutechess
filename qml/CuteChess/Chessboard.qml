import QtQuick 1.1
import Qt3D 1.0
import Qt3D.Shapes 1.0
import "DrawingConstants.js" as Graphics

Item3D {
    id: root

    Cube {
        effect: Effect { texture: "textures/chessboard_wood.jpg" }
        transform: Scale3D { scale: Qt.vector3d(Graphics.BoardWidth, Graphics.BoardHeight, Graphics.BoardWidth); }
    }

    Repeater {
        model: 8
        Repeater {
            model: 8
            property int row: index
            Quad {
                effect: Effect { texture: ((row + index) % 2 === 0) ? "textures/blackCell" : "textures/whiteCell" }
                position: Qt.vector3d(Graphics.cellCenter_X(row), Graphics.BoardTopSurfaceOffset + 0.01, Graphics.cellCenter_Z(index));
                transform: Scale3D { scale: Qt.vector3d(Graphics.BoardCellSpacing, 1.0, Graphics.BoardCellSpacing); }
            }
        }
    }

    Cube {
        effect: Effect { texture: "textures/chessboard_wood.jpg" }
        transform: Scale3D { scale: Qt.vector3d(Graphics.BoardWidth + 4.0, Graphics.BoardHeight * 1.2, 4.0); }
        position: Qt.vector3d(2.0, Graphics.BoardHeight * 0.1, -Graphics.BoardWidth / 2.0 - 2.0)
    }

    Cube {
        effect: Effect { texture: "textures/chessboard_wood.jpg" }
        transform: Scale3D { scale: Qt.vector3d(Graphics.BoardWidth + 4.0, Graphics.BoardHeight * 1.2, 4.0); }
        position: Qt.vector3d(-2.0, Graphics.BoardHeight * 0.1, Graphics.BoardWidth / 2.0 + 2.0)
    }

    Cube {
        effect: Effect { texture: "textures/chessboard_wood.jpg" }
        transform: Scale3D { scale: Qt.vector3d(4.0, Graphics.BoardHeight * 1.2, Graphics.BoardWidth + 4.0); }
        position: Qt.vector3d(-Graphics.BoardWidth / 2.0 - 2.0, Graphics.BoardHeight * 0.1, -2.0)
    }

    Cube {
        effect: Effect { texture: "textures/chessboard_wood.jpg" }
        transform: Scale3D { scale: Qt.vector3d(4.0, Graphics.BoardHeight * 1.2, Graphics.BoardWidth + 4.0); }
        position: Qt.vector3d(Graphics.BoardWidth / 2.0 + 2.0, Graphics.BoardHeight * 0.1, 2.0)
    }
}
