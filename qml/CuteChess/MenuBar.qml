// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: menubar
    width: 100
    height: 600

    property bool rotate3Denabled : false
    property bool topViewEnabled: false
    property bool showThreatsEnabled: true

    signal newGameClicked()

    Rectangle {
        width: parent.height
        height: parent.width
        anchors.centerIn: parent
        rotation: 90
        gradient: Gradient {
            GradientStop {
                position: 0.00;
                color: "#8f8989";
            }
            GradientStop {
                position: 1.00;
                color: "#313030";
            }
        }
    }

    Column {
        anchors.top: menubar.top
        anchors.bottom: exitItem.top
        anchors.left: menubar.left
        anchors.right: menubar.right
        anchors.topMargin: 15
        spacing: 15

        MenuItem {
            iconSource: "textures/newGame.png"
            text: qsTr("New game")
            onClicked: menubar.newGameClicked()
        }

        MenuItem {
            iconSource: "textures/rotate3D.png"
            text: qsTr("Rotate 3D")
            checked: menubar.rotate3Denabled
            onClicked: menubar.rotate3Denabled = !menubar.rotate3Denabled
        }

        MenuItem {
            iconSource: "textures/topView.png";
            text: qsTr("Top view")
            checked: menubar.topViewEnabled
            onClicked: menubar.topViewEnabled = !menubar.topViewEnabled
        }

        MenuItem {
            iconSource: "textures/threats.png"
            text: qsTr("Threats")
            checked: menubar.showThreatsEnabled
            onClicked: menubar.showThreatsEnabled = !menubar.showThreatsEnabled
        }
    }

    MenuItem {
        id: exitItem
        iconSource: "textures/exit.png"
        text: qsTr("Exit")

        anchors.bottom: menubar.bottom
        anchors.bottomMargin: 15

        onClicked: Qt.quit()
    }

}
