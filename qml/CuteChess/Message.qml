// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    property alias text: textField.text
    width: textField.width
    height: textField.height

    Text {
        id: textField
        horizontalAlignment: Text.AlignHCenter
        color: "#ffffff"
        style: Text.Outline
        font.family: "MV Boli"
        font.bold: false
        font.pointSize: 60
    }

//    NumberAnimation {
//        id: animateOpacity
//        target: textField
//        properties: "opacity"
//        from: 0.5
//        to: 1.0
//        duration: 250
//        easing.type: Easing.InBounce
//    }
}
