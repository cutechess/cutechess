// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import Qt3D.Shapes 1.0
import "DrawingConstants.js" as Graphics

Item3D {
    id: root
    property int row
    property int column
    property color color: "lime"
    property real mainQuadScale: 0.8

    Quad {
        id: mainQuad
        effect: Effect {
            id: effect
            color: root.color
//            blending: true
        }

        scale: Graphics.BoardCellSpacing * mainQuadScale

        x: Graphics.cellCenter_X(column)
        y: Graphics.BoardTopSurfaceOffset + 0.02
        z: Graphics.cellCenter_Z(row)

        onClicked: root.clicked()
        onHoverEnter: root.hoverEnter()
        onHoverLeave: root.hoverLeave()
    }

    SequentialAnimation {
        running: true
        loops: Animation.Infinite

        NumberAnimation {
            target: root
            property: "mainQuadScale"
            from: 0.7
            to: 0.9
            duration: 750
        }

        NumberAnimation {
            target: root
            property: "mainQuadScale"
            from: 0.9
            to: 0.7
            duration: 750
        }
    }


//    Repeater {
//        id: ticksRepeater
//        model: 12
//        Quad {
//            id: ticks
//            scale: Graphics.BoardCellSpacing * (1 - mainQuadScale) / 2.1
//            property real offset: Graphics.BoardCellSpacing / 2.0 - (Graphics.BoardCellSpacing * (1 - mainQuadScale)) / 4.0

//            x: Graphics.cellCenter_X(column) - offset
//            y: Graphics.BoardTopSurfaceOffset + 0.02
//            z: Graphics.cellCenter_Z(row) + offset

//            effect: Effect {
//                color: root.color
//                blending: false
//            }

//            SequentialAnimation {
//                running: true
//                PauseAnimation { duration: 2000 / ticksRepeater.count * index }
//                SequentialAnimation {
//                    loops: Animation.Infinite
//                    NumberAnimation {
//                        target: ticks
//                        property: "z"
//                        from: Graphics.cellCenter_Z(row) + offset
//                        to: Graphics.cellCenter_Z(row) - offset
//                        duration: 500
//                    }
//                    NumberAnimation {
//                        target: ticks
//                        property: "x"
//                        from: Graphics.cellCenter_X(column) - offset
//                        to: Graphics.cellCenter_X(column) + offset
//                        duration: 500
//                    }
//                    NumberAnimation {
//                        target: ticks
//                        property: "z"
//                        from: Graphics.cellCenter_Z(row) - offset
//                        to: Graphics.cellCenter_Z(row) + offset
//                        duration: 500
//                    }
//                    NumberAnimation {
//                        target: ticks
//                        property: "x"
//                        from: Graphics.cellCenter_X(column) + offset
//                        to: Graphics.cellCenter_X(column) - offset
//                        duration: 500
//                    }
//                }
//            }
//        }
//    }
}
