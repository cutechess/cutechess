// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Qt3D 1.0
import Qt3D.Shapes 1.0
import CuteChess 1.0
import "ChessState.js" as Logic


Item3D {
    id: root
    property bool isWhiteToPlay: true
    property bool whiteCheck: false
    property bool blackCheck: false
    property bool checkmate: false
    property bool pat: false
    property bool showThreatsEnabled: true
    signal choosePromotionPiece()

    Chessboard {
    }

    ChessEngine {
        id: game
    }

    Binding { target: root; property: "isWhiteToPlay"; value: game.isWhiteToPlay }
    Binding { target: root; property: "whiteCheck"; value: game.whiteCheck }
    Binding { target: root; property: "blackCheck"; value: game.blackCheck }
    Binding { target: root; property: "checkmate"; value: game.checkmate }
    Binding { target: root; property: "pat"; value: game.pat }

    // Draw available positions markers
    Repeater {
        id: availableMarkers
        model: game.getAvailablePositionModel()
        AvailablePosition {
            row: model.row
            column: model.column
            color: "aqua"

            onClicked: {
                if (!Logic.isSelectionValid()) return;
                moveSelectedTo(row, column);
            }

            onHoverEnter: game.setThreatMovingPiece(Logic.selectedPosition_x, Logic.selectedPosition_y, row, column);
            onHoverLeave: game.clearThreats();
        }
    }

    // Draw threat positions markers
    Repeater {
        id: threatMarkers
        model: showThreatsEnabled ? game.getThreatPositionModel() : null
        AvailablePosition {
            row: model.row;
            column: model.column
            color: "#90FF0000"
        }
    }

    function moveSelectedTo(x, y) {
        Logic.move_x = x;
        Logic.move_y = y;

//        console.log("pieceTypeat : " + game.pieceTypeAt(Logic.selectedPosition_x, Logic.selectedPosition_y));
//        console.log("pieceWhite at :" + game.pieceIsWhiteAt(Logic.selectedPosition_x, Logic.selectedPosition_y));

        // Check if a promotion is going to occurs
        if (game.pieceTypeAt(Logic.selectedPosition_x, Logic.selectedPosition_y) === PieceModel.Pawn &&
                ((game.pieceIsWhiteAt(Logic.selectedPosition_x, Logic.selectedPosition_y) && x === 7) || (!game.pieceIsWhiteAt(Logic.selectedPosition_x, Logic.selectedPosition_y) && x === 0))) {
            // Show the dialog for selection of promotion
            choosePromotionPiece();

            // Delay real move until user validate the choice
            return;
        }

        makeMoveSelectedTo(0);
    }

    function makeMoveSelectedTo(promotion) {
        var promotePiece = PieceModel.Queen;
        if (promotion === 0) promotePiece = PieceModel.Knight;
        if (promotion === 1) promotePiece = PieceModel.Bishop;
        if (promotion === 2) promotePiece = PieceModel.Rook;

        console.log("promotion and promotePiece :" + promotion + " - " + promotePiece);
        game.movePiece(Logic.selectedPosition_x, Logic.selectedPosition_y, Logic.move_x, Logic.move_y, promotePiece);
        Logic.clearSelectedPosition();
        game.setSelectedPosition(-1, -1);
        game.clearThreats();
    }

    Repeater {
        model: game.getAllPieces()
        PieceDelegate {
            pieceModel: modelData

            onPieceClicked: {
                // Do not interract with captured pieces anymore
                if (modelData.captured) return;

                // Resolve captures
                if (Logic.isSelectionValid() && game.isAvailableMove(modelData.position.x, modelData.position.y)) {
                    moveSelectedTo(modelData.position.x, modelData.position.y);
                    return;
                }

                // Check we click on our piece.
                if (modelData.isWhite !== game.isWhiteToPlay) return;

                // Select the piece
                if (Logic.setSelectedPosition(modelData.position.x, modelData.position.y)) {
                    game.setSelectedPosition(modelData.position.x, modelData.position.y);
                }

                // Mark pieces that threaten this one (player help purpose)
                game.setThreatPosition(modelData.position.x, modelData.position.y);
            }

            onPieceHoverEnter: {
                if (Logic.isSelectionValid && modelData !== null && game.isAvailableMove(modelData.position.x, modelData.position.y)) {
                    game.setThreatMovingPiece(Logic.selectedPosition_x, Logic.selectedPosition_y, modelData.position.x, modelData.position.y);
                }
            }

            onPieceHoverLeave: game.clearThreats();
        }
    }
}
