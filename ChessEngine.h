#ifndef CHESSENGINE_H
#define CHESSENGINE_H

#include <QVector>
#include "Piece.h"

class PositionListModel;

class ChessEngine : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isWhiteToPlay READ isWhiteToPlay NOTIFY isWhiteToPlayChanged)
    Q_PROPERTY(bool whiteCheck READ whiteCheck NOTIFY whiteCheckChanged)
    Q_PROPERTY(bool blackCheck READ blackCheck WRITE setBlackCheck NOTIFY blackCheckChanged)
    Q_PROPERTY(bool checkmate READ checkmate NOTIFY checkmateChanged)
    Q_PROPERTY(bool pat READ pat NOTIFY patChanged)

public:
    explicit ChessEngine(QObject *parent = 0);
    virtual ~ChessEngine();

    bool isWhiteToPlay() const { return m_isWhiteToPlay; }
    bool whiteCheck() const { return m_whiteCheck; }
    bool blackCheck() const { return m_blackCheck; }
    bool checkmate() const { return m_checkmate; }
    bool pat() const { return m_pat; }
    Piece* pieceAt(const QPoint& position) const;
    Piece* pieceAt(int x, int y) const;
    bool isPositionFree(const QPoint& point) const;

    Q_INVOKABLE QList<QObject*> getAllPieces() const;
    Q_INVOKABLE QList<QPoint> availablePositionsFrom(int x, int y);
    Q_INVOKABLE PositionListModel* getAvailablePositionModel() const { return m_availablePositionsModel; }
    Q_INVOKABLE PositionListModel* getThreatPositionModel() const { return m_threatPositionsModel; }
    Q_INVOKABLE bool isAvailableMove(int x, int y);
    Q_INVOKABLE int pieceTypeAt(int x, int y);
    Q_INVOKABLE bool pieceIsWhiteAt(int x, int y);

    QList<QPoint> availablePositionsFrom(Piece *piece);
    QList<QPoint> threatList(Piece *threatenedPiece);
    QList<QPoint> threatList(const QPoint& threatenedPosition, bool threatenedIsWhite);
    QList<QPoint> threatList(Piece *threatenedPiece, Piece* movingPiece, const QPoint &movingTo);
    QList<QPoint> threatList(const QPoint& threatenedPosition, bool threatenedIsWhite, Piece* movingPiece, const QPoint &movingTo);

    bool movePieceTo(Piece* piece, const QPoint& pos, Piece::ChessType promoteType = Piece::Queen);

signals:
    void isWhiteToPlayChanged(bool arg);
    void whiteCheckChanged(bool check);
    void blackCheckChanged(bool check);
    void checkmateChanged(bool checkmate);
    void patChanged(bool arg);

public slots:
    void setSelectedPosition(int x, int y);
    void setThreatPosition(int x, int y);
    void setThreatMovingPiece(int posx, int posy, int movingx, int movingy);
    void clearThreats();
    void movePiece(int xFrom, int yFrom, int xTo, int yTo, int promoteType = Piece::Queen);


private slots:
    void setWhiteToPlay(bool arg);
    void setWhiteCheck(bool arg);
    void setBlackCheck(bool arg);
    void setCheckmate(bool arg);
    void setPat(bool arg);

private:
    void setPieceAt(const QPoint& point, Piece *piece);
    void capturePosition(const QPoint& position);
    bool isCaptureEnPassant(const Piece *piece, const QPoint& movingTo) const;
    QList<Piece*> movePieceAndCapture(Piece* piece, const QPoint& pos, bool fakeMove = false);
    bool isSmallRoque(bool isWhite);
    bool isBigRoque(bool isWhite);
    bool isFreeOrOppositeColor(const QPoint& move, bool isWhite) const;
    bool hasAvailableMove(bool white);

    // List basic moves availables
    QList<QPoint> rookAvailablePositionsFrom(const Piece* piece) const;
    QList<QPoint> bishopAvailablePositionsFrom(const Piece* piece) const;
    QList<QPoint> knightAvailablePositionsFrom(const Piece* piece) const;
    QList<QPoint> kingAvailablePositionsFrom(const Piece* piece) const;
    QList<QPoint> pawnMovePositionsFrom(const Piece* piece) const;
    QList<QPoint> pawnCapturePositionsFrom(const Piece* piece) const;

private:
    QVector< QVector<Piece*> > m_board;
    QList<Piece*> m_capturedPieces;
    Piece* m_whiteKing;
    Piece* m_blackKing;
    Piece* m_selectedPiece;
    QPoint m_pawnMakesFirstMove;
    bool m_isWhiteToPlay;
    bool m_whiteCheck;
    bool m_blackCheck;
    bool m_checkmate;
    bool m_pat;
    PositionListModel* m_availablePositionsModel;
    PositionListModel* m_threatPositionsModel;
};

#endif // CHESSENGINE_H
