#include "PositionListModel.h"

PositionListModel::PositionListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles;
    roles[Qt::UserRole + 1] = "row";
    roles[Qt::UserRole + 2] = "column";
    roles[Qt::UserRole + 3] = "position";
    setRoleNames(roles);
}

int PositionListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return m_positions.count();
}

QVariant PositionListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();
    if (index.row() < 0 || index.row() >= m_positions.count()) return QVariant();

    if (role == Qt::UserRole + 1) return m_positions.at(index.row()).x();
    if (role == Qt::UserRole + 2) return m_positions.at(index.row()).y();
    if (role == Qt::UserRole + 3) return m_positions.at(index.row());
    if (role == Qt::DisplayRole) return QString("[%1,%2]").arg(m_positions.at(index.row()).x()).arg(m_positions.at(index.row()).y());

    return QVariant();
}

void PositionListModel::setPositions(QList<QPoint> positions)
{
    beginResetModel();

    m_positions = positions;

    endResetModel();
}

bool PositionListModel::contains(const QPoint &pos) const
{
    return m_positions.contains(pos);
}

void PositionListModel::clear()
{
    beginResetModel();

    m_positions.clear();

    endResetModel();
}
