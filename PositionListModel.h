#ifndef POSITIONLISTMODEL_H
#define POSITIONLISTMODEL_H

#include <QAbstractListModel>
#include <QPoint>

class PositionListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit PositionListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    void setPositions(QList<QPoint> positions);
    bool contains(const QPoint& pos) const;
    
public slots:
    void clear();

private:
    QList<QPoint> m_positions;
    
};

#endif // POSITIONLISTMODEL_H
