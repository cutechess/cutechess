#ifndef PIECE_H
#define PIECE_H

#include <QObject>
#include <QPoint>

class Piece : public QObject
{
    Q_OBJECT
    Q_ENUMS(ChessType)
    Q_PROPERTY(ChessType type READ type NOTIFY typeChanged)
    Q_PROPERTY(QPoint position READ position NOTIFY positionChanged)
    Q_PROPERTY(bool isWhite READ isWhite NOTIFY isWhiteChanged)
    Q_PROPERTY(bool captured READ captured NOTIFY capturedChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

public:
    enum ChessType {
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen,
        King,
        Undefined
    };

public:
    static bool isPositionValid(const QPoint& position);

public:
    explicit Piece(QObject *parent = 0);
    explicit Piece(ChessType type, const QPoint& pos, bool isWhite, QObject *parent = 0);
    virtual ~Piece();
    
    ChessType type() const { return m_type; }
    QPoint position() const { return m_position; }
    QPoint lastPosition() const { return m_lastPosition; }
    bool isWhite() const { return m_isWhite; }
    bool isFirstMove() const { return !isPositionValid(m_lastPosition); }
    bool captured() const { return !isPositionValid((m_position)); }
    bool selected() const { return m_selected; }

    void setType(ChessType arg);
    void moveTo(const QPoint& position);
    void capture();
    void rollback();

public slots:
    void setSelected(bool arg);

signals:
    void typeChanged(Piece::ChessType arg);
    void positionChanged(QPoint arg);
    void isWhiteChanged(bool arg);
    void capturedChanged(bool arg);
    void selectedChanged(bool arg);

private:
    void setPosition(QPoint arg);

private:
    ChessType m_type;
    QPoint m_position;
    QPoint m_lastPosition;
    QPoint m_lastLastPosition;
    bool m_isWhite;
    bool m_selected;
};

/**
  * if position is not valid it means the piece has been captured
  * if last position is not valid it means the piece has not moved yet
  */

#endif // PIECE_H
